# React From scratch Project

## Setup:

    git clone https://gitlab.com/chokri.mechichi/react-from-scratch-demo
    npm i
    npm start

## Project boilerplate:

    React/Typescript
    Redux/ReduxTollkit

    Module bundler:
        webpack v5

    Transcompiler:
        babel v7

    Loaders:
        style-loader
        css-loader
        sass-loader

    Linter:
        ESlint v8

    Testing Lib:
        Jest/ts-jest
        @testing-library/react

    NetworkingLib:
        Axios

## Project Folder Structure

    /webpack (webpack config files Dev | Prod )
    /app
    /features
    /services
    /slices
    /ui
        /_settings
        /atoms
        /molecules
        /organisms
        /templates
        /pages

## Available Scripts

Run :

### `npm start`

Build

### `npm run build`

Test :

### `npm test`

Lint :

### `npm run lint`

Formatter :

### `npm run format`
