import React from 'react';

type Prop = {
    children? : React.ReactNode
}

const LoginTemplate = ({children}:Prop) => {
    return (
        <div className='flex-center-container'>
            {children}
        </div>
    );
};

export default LoginTemplate;