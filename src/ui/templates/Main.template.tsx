import React from 'react'
import Button, { ButtonVariant } from '../atoms/button/Button'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { NavLink } from 'react-router-dom'

type Prop = {
  children?: React.ReactNode
}

const MainTemplate = ({ children }: Prop) => {
  return (
    <div className="flex-center-container">
      <div className="row my-2">
        <div className="col-50">
          <NavLink to={'/app'}>
            <Button>
              <FontAwesomeIcon icon={['fas', 'user']} />
            </Button>
          </NavLink>
        </div>
        <div className="col-50">
          <NavLink to={'/app/calendar'}>
            <Button variant={ButtonVariant.DEFAULT}>
              <FontAwesomeIcon icon={['fas', 'calendar-week']} />
            </Button>
          </NavLink>
        </div>
      </div>

      <div className="row center-flex-h">{children}</div>
    </div>
  )
}

export default MainTemplate
