import React, { useState } from 'react'
import Input from '../../atoms/input/Input'
import Button, { ButtonVariant, ButtonType } from '../../atoms/button/Button'
import Card from '../../atoms/card/Card'
import Alert, { AlertVariant } from '../../atoms/alerts/Alert'
import { Formik, Form, Field } from 'formik'
import * as Yup from 'yup'

type Props = {
  onSubmit: (data: any) => void
  loading: boolean
  error: string | string[]
}
const LoginForm = ({ onSubmit, error, loading }: Props) => {
  const SignupSchema = Yup.object().shape({
    username: Yup.string().min(4, 'Too Short!').required('Username Required'),
    password: Yup.string().min(6, 'Too Short!').required('Password Required'),
  })

  return (
    <Card className="p-4 w-min-50">
      <h3 className="text-center">Login</h3>

      <Formik
        initialValues={{ username: '', password: '' }}
        validationSchema={SignupSchema}
        onSubmit={(values) => {
          onSubmit({ username: values?.username, password: values?.password })
        }}
      >
        {({ errors, touched }) => (
          <Form>
            {error && (
              <Alert varriant={AlertVariant.WARN}>
                {JSON.stringify(error)}
              </Alert>
            )}
            <label htmlFor="username">Username</label>
            <Input name="username" isFormik={true} />
            <div>{errors.username}</div>
            {errors.username && (
              <Alert varriant={AlertVariant.WARN}>{errors.username}</Alert>
            )}

            <label htmlFor="password" className="mt-2">
              Password
            </label>
            <Input name="password" _type="password" isFormik={true} />
            {errors?.password && (
              <Alert varriant={AlertVariant.WARN}>{errors.password}</Alert>
            )}
            <hr />
            <Alert varriant={AlertVariant.INFO}>
              You can login with <b>Username : </b>kminchelle <b>Password : </b>
              0lelplR
            </Alert>

            <Button className="mt-4" loading={loading} type="submit">
              Log In
            </Button>
          </Form>
        )}
      </Formik>
    </Card>
  )
}

const defaultProps = {
  onSubmit: (data: any) => {
    console.log(data)
  },
}
LoginForm.defaultProps = defaultProps

export default LoginForm
