import React , {useState} from 'react';
import CalendarPicker from '../../atoms/calendar/Calendar';
import Input from '../../atoms/input/Input';
import Button , {ButtonVariant} from '../../atoms/button/Button';
import Card from '../../atoms/card/Card';

const ScheduleForm = () => {
    const[date , set_date] = useState(new Date())
    return (
        <Card>
            <div className='row'>
                <h3 className='fnt-w5'>Schedule Response</h3>
                <div className='row'>
                    <div className='col-50'>
                        <div className="row">
                            <div className="col-25 center-flex-v ps-1">
                                <span className='fnt-w6 text-sm text-muted'> Date</span>
                            </div>
                            <div className="col-75">
                                <Input disabled={true} className='text-sm' value={date.toLocaleString("lookup").substring(0,10)}/>
                            </div>
                        </div>
                    </div>

                    <div className='col-50'>
                        <div className="row">
                            <div className="col-25 center-flex-v ps-1">
                                <span className='fnt-w6 text-sm text-muted'>Time</span>
                            </div>
                            <div className="col-75">
                                <Input disabled={true}  className='text-sm' value={date.toLocaleString("en-US").substring(10,22)} />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row mt-1">
                    <CalendarPicker value={date} onChange={set_date} />
                </div>
                <div className="row mt-2">
                    <div>
                    <Button className='me-2'>Schedule</Button>
                    <Button variant={ButtonVariant.DEFAULT}>Cancel</Button>
                    </div>
                </div>
                
            </div>
        </Card>
    );
};

export default ScheduleForm;