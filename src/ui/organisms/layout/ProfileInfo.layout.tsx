import React from 'react'
import { User } from '../../../types/entity/user'
import Card from '../../atoms/card/Card'
import Iconlabel from '../../atoms/label/InconLabel'
import Avatar from '../../atoms/avatar/Avatar'
import Button from '../../atoms/button/Button'
type Props = {
  user?: User | null
}
const ProfileInfoLayout = ({ user }: Props) => {
  return (
    <Card className="p-4 w-50">
      <div className="row">
        <h3>
          <span>Profile</span>
        </h3>
        <div className="row">
          <div className="col-50">
            <Iconlabel icon="user">
              <div className="row">
                <span>{`${user?.firstName} ${user?.lastName}`}</span>
              </div>
              <div className="row">
                <span className="text-muted">xxx</span>
              </div>
            </Iconlabel>
          </div>
          <div className="col-50">
            <Avatar src={user?.image} className="float-e" />
          </div>
        </div>

        <Iconlabel icon="envelope" className="al-base">
          {user?.email}
        </Iconlabel>
        <Iconlabel icon="venus-mars" className="al-base">
          {user?.gender}
        </Iconlabel>
        <Iconlabel icon="phone-alt" className="al-base">
          {user?.phone || '99 9999 999'}
        </Iconlabel>
        <div className="mt-2">
          <Button className="float-e">Log-out</Button>
        </div>
      </div>
    </Card>
  )
}

export default ProfileInfoLayout
