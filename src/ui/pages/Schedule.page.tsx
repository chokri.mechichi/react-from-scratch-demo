import React, { useEffect } from 'react'
import MainTemplate from '../templates/Main.template'
import ScheduleForm from '../organisms/form/Schedule.form'

const SchedulePage = () => {
  useEffect(() => {
    console.log('SHED PAGE')
  }, [])
  return (
    <MainTemplate>
      <div className="col-50">
        <ScheduleForm />
      </div>
    </MainTemplate>
  )
}

export default SchedulePage
