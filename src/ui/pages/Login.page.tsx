import React from 'react';
import LoginTemplate from '../templates/Login.template';
import LoginForm from '../organisms/form/Login.form';

type Props ={
    onLogin : (data : any)=>void
    loading : boolean
    error : string|string[]
    
}
const LoginPage = ({onLogin , error , loading} :Props) => {
    return (
        <LoginTemplate>
            <LoginForm onSubmit={onLogin} error={error} loading={loading} />
        </LoginTemplate>
    );
};

export default LoginPage;