import React from 'react'
import MainTemplate from '../templates/Main.template'
import ProfileInfoLayout from '../organisms/layout/ProfileInfo.layout'
import { User } from '../../types/entity/user'

type Props = {
  user?: User | null
}

const ProfilePage = ({ user }: Props) => {
  return (
    <MainTemplate>
      <div className="col-100">
        <ProfileInfoLayout user={user} />
      </div>
    </MainTemplate>
  )
}

export default ProfilePage
