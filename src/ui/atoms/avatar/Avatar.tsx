import React from 'react';
import styles from './Style.module.css'
import classNames from 'classnames';

type Props = {
    src? : string
    className? : string
}
const Avatar = ({src , className}:Props) => {
    return (
        <img src={src} className={classNames(styles.avatar , className)} loading='lazy' alt="" />
    );
};

export default Avatar;