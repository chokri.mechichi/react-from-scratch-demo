import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import classNames from 'classnames'

type Props = {
  children: React.ReactNode
  icon: string
  className?: string
}
const Iconlabel = (props: Props) => {
  const { children, icon, className } = props
  return (
    <div className={classNames('row my-2 fnt-w4', className)}>
      <div className="col-auto">
        <span className="me-2 text-gray-dark">
          <FontAwesomeIcon icon={['fas', icon as any]} />
        </span>
      </div>
      <div className="col-75">{children}</div>
    </div>
  )
}

export default Iconlabel
