import React , {useState , useEffect} from 'react';
import 'react-calendar/dist/Calendar.css';
import Calendar from 'react-calendar';
import TimeItem from './TimeItem';
import './Styles.css'
const hours = Array.from(Array(24).keys())

type Props = {
    value : Date
    onChange : (d:Date)=>void
}
const CalendarPicker = ({value , onChange}:Props) => {

    const setHours = (v:number) => {
        let d = new Date(value.getTime())
        d.setHours(v,0,0)
        onChange(d)
    }
    
    return (
        <div className="row">
                    <div className="col-50">
                        <Calendar onChange={onChange} />
                    </div>

                    <div className="col-50 time_list">
                       {
                        hours.map(el => <TimeItem value={el}  isSelected={el == value.getHours()} onClick={setHours} key={`itm-time${el}`} />)
                       }
                    </div>
        </div>
    );
};

export default CalendarPicker;