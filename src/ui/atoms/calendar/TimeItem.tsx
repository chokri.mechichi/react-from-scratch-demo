import React from 'react';
import './Styles.css'
import classnames from 'classnames'

type Props = {
    value : number
    isSelected : boolean
    onClick : (v:number)=>void
}
const TimeItem = ({value  , isSelected , onClick}:Props) => {
    return (
        <div className={classnames('row time_item p-2 text-center' , {'time_item_active':isSelected})} onClick={e => onClick(value)}>
            <span>
                {
                    (value >= 12)?
                    (`${(value-12)<10 ? `0${value-12}`:`${value-12}`}:00 PM`)
                    :
                    (`${value<10 ? `0${value}`:`${value}`}:00 AM`)
                }
            </span>

        </div>
    );
};

export default TimeItem;