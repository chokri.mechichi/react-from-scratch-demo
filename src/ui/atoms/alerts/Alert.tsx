import React from 'react';
import styles from './Style.module.css'
import classNames from 'classnames';

export const AlertVariant = {
    WARN: 'alert_warning',
    INFO: 'alert_secondary',
}

type Props = {
    children? : React.ReactNode
    varriant? : string
}
const Alert = ({children , varriant}:Props) => {
    return (
        <div className={classNames(styles[varriant])}>
            {children}
        </div>
    );
};

const defaultProps = {
    variant: AlertVariant.INFO,
}
Alert.defaultProps = defaultProps

export default Alert;