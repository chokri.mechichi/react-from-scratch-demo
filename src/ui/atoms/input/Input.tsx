import React from 'react';
import styles from './Style.module.css'
import classnames from 'classnames'
import { Field } from 'formik';

type Props = {
    _type : string
    value? : string | number
    placeholder? : string
    onChangeHandler : ( value : string|number)=>void
    className? : string
    name? : string
    disabled? : boolean
    isFormik? : boolean
}

const Input = ({_type , placeholder , value , onChangeHandler , className , name , disabled , isFormik} : Props) => {
    return (
        (
        isFormik ?
        <Field name={name} disabled={disabled} type={_type} placeholder={placeholder} value={value}  className={classnames(styles.form_input , className)} />
        :
        <input disabled={disabled} type={_type} placeholder={placeholder} value={value} onChange={e =>onChangeHandler(e.target.value)} className={classnames(styles.form_input , className)} />
        )
        );
};

const defaultProps = {
    _type : 'text',
    placeholder : '',
    onChangeHandler : (value : string|number)=>{},
    className : '',
    name : '',
}
Input.defaultProps = defaultProps

export default Input;