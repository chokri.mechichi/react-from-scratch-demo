import React from 'react';
import styles from './Style.module.css'
import classNames from 'classnames';

type Prop = {
    children? : React.ReactNode
    className? : string
}
const Card = ({children , className}:Prop) => {
    return (
        <div className={classNames(styles.card , className)}>
            {children}
        </div>
    );
};

export default Card;