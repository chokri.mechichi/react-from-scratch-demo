import React from 'react';
import styles from './Style.module.css'
import classNames from 'classnames';

export const ButtonType = {
    BUTTON: 'button',
    RESET: 'reset',
    SUBMIT: 'submit',
}
export const ButtonVariant = {
    DEFAULT: 'btn_default',
    PRIMARY: 'btn_primary',
}

type Prop = {
    variant? : string
    size? : string
    className? : string
    loading : boolean
    children : React.ReactNode
    onClick: () => void,
    type : 'button' | 'reset' | 'submit'
}
const Button = ({variant , size , className , children , loading , onClick , type}  : Prop) => {
    return (
        <button type={type} className={classNames(styles.btn , styles[variant] , className)} disabled={loading}>{children}</button>
    );
};

const defaultProps = {
    type: ButtonType.BUTTON,
    variant: ButtonVariant.PRIMARY,
    onClick: () => { },
    className: '',
    loading: false,
}
Button.defaultProps = defaultProps

export default Button;