import React , {useEffect, useState} from 'react';
import { useAppSelector, useAppDispatch } from '../../app/hooks';
import { doLogin , clear , selectLoading , selectError , selectLoggedUser } from '../../slices/auth.slice';
import { useNavigate } from "react-router-dom";
import LoginPage from '../../ui/pages/Login.page';
type LoginFormData = {
    username : string
    password : string
}
const LoginContainer = () => {

    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const loading = useAppSelector(selectLoading);
    const loggedUser = useAppSelector(selectLoggedUser);
    const error = useAppSelector(selectError);

    const onSubmitLogin = (data : LoginFormData)=> {
        if(data)
         dispatch(doLogin(data))
    }

    useEffect(()=>{
        if(loggedUser)
          navigate('/app')
    },[loggedUser])


    return (
    
        <LoginPage loading={loading} error={error} onLogin={onSubmitLogin} />
  
    );
};

export default LoginContainer;