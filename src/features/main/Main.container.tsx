import React, { useEffect } from 'react'
import { useAppSelector, useAppDispatch } from '../../app/hooks'
import {
  selectLoggedUser,
  selectIsLoggedIn,
  clearLoggedUserStorage,
} from '../../slices/auth.slice'
import { Routes, Route, useNavigate, Outlet } from 'react-router-dom'
import ProfilePage from '../../ui/pages/Profile.page'
import SchedulePage from '../../ui/pages/Schedule.page'
const MainContainer = () => {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()
  const loggedUser = useAppSelector(selectLoggedUser)
  const isLoggedIn = useAppSelector(selectIsLoggedIn)

  useEffect(() => {
    if (!isLoggedIn) navigate('/login')
  }, [isLoggedIn])

  const logout = () => {
    dispatch(clearLoggedUserStorage())
  }

  return (
    <>
      <Routes>
        <Route index element={<ProfilePage user={loggedUser} />} />
        <Route path="calendar" element={<SchedulePage />} />
      </Routes>
    </>
  )
}

export default MainContainer
