//API BASE URL 
export const API_BASE_URL = 'https://dummyjson.com'
//API FILE URL 
export const API_FILES_URL = 'https://robohash.org'

//IT WILL BE USED TO GET TOKEN FROM LOCAL STORAGE
export const TOKEN_NAME = 'rfsd-token'

//IT WILL BE USED TO GET USER SESSION DATA FROM LOCAL STORAGE
export const SESSION_NAME = 'rfsd-user-data'