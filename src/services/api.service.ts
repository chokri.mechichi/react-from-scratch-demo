import axios from "axios";
import { API_BASE_URL , TOKEN_NAME , SESSION_NAME } from "./config";
import { getItemByName } from "./Storage";


export function login(data:any) {
    return axios.post(`${API_BASE_URL}/auth/login`, data);
}

export function updateProfile(data:any) {
    const token = getItemByName(TOKEN_NAME)
    const user = getItemByName(TOKEN_NAME)
     console.log(token)
    return axios.put(`${API_BASE_URL}/users/${user?.id}`, data , {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}