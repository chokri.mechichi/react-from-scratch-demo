export function getItemByName(name : string) {
    try {
        return JSON.parse(localStorage.getItem(name) || '');
    } catch (error) {
        return null
    }
    
};
export function setItemByName(data : any , name : string) {
    localStorage.setItem(name, JSON.stringify(data));
}
export function removeItemByName(name : string) {
    localStorage.removeItem(name);
}
