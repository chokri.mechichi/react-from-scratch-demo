import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState, AppThunk } from '../app/store';
import { login , updateProfile } from '../services/api.service';
import { setItemByName , getItemByName , removeItemByName } from '../services/Storage';
import { TOKEN_NAME , SESSION_NAME } from '../services/config';
import { User } from '../types/entity/user';


export const doLogin = createAsyncThunk(
    'auth/login',
    async (data : any, { rejectWithValue }) => {
        try {
            const res : any = await login(data);
            return res.data;
        } catch (error: any) {
            const message = (error.response && error.response.data && error.response.data.message) || error.message || error.toString();
            return rejectWithValue(message)
        }
    }
);


export interface AuthState {
    loading: boolean 
    data: any   // it will containe the payload of API response (success)
    error: string | string[]  // it will containe the payload of API error msg ( fail)
    loggedUser? : User | null
    isLoggedIn : boolean
}

const initialState: AuthState = {
    loading: false,
    data: null,
    error: '',
    loggedUser : null,
    isLoggedIn : false

};

export const authSlice = createSlice({
    name: 'Auth',
    initialState,
    reducers : {
        clear: (state) => {
            Object.assign(state, initialState)
        },
        //Load logged user from local storage & update isLoggedIn state
        loadLoggedUser: (state) => {
            const u : User = getItemByName(SESSION_NAME)
            const token = getItemByName(TOKEN_NAME)
            state.loggedUser = u

            if(u && token)
              state.isLoggedIn = true
            else
              state.isLoggedIn = false
        },
        clearLoggedUserStorage: (state) => {
            removeItemByName(TOKEN_NAME)
            removeItemByName(SESSION_NAME)
            Object.assign(state, initialState)
        },
    } ,
    extraReducers: (builder) => {
        builder
            .addCase(doLogin.pending, (state) => {
                state.loading = true;
                state.error = '';
                state.loggedUser = null ;
                state.isLoggedIn = false ;
            })
            .addCase(doLogin.fulfilled, (state, action) => {
                setItemByName(action.payload?.token , TOKEN_NAME)
                //setItemByName(action.payload?.refreshToken , REFRESH_TOKEN_NAME)
                setItemByName((action.payload as User) , SESSION_NAME)
                state.loading = false;
                state.error = '';
                state.loggedUser = action.payload ;
                state.isLoggedIn = true ;
            })
            .addCase(doLogin.rejected, (state , action) => {
                state.loading = false;
                state.error = action.payload as string;
            });
    }
})

//SELECT STATES HELPER
export const selectLoading = (state: RootState) => state._auth.loading;
export const selectLoggedUser = (state: RootState) => state._auth.loggedUser;
export const selectIsLoggedIn = (state: RootState) => state._auth.isLoggedIn;
export const selectError = (state: RootState) => state._auth.error;

export const { clear , loadLoggedUser , clearLoggedUserStorage} = authSlice.actions;
export default authSlice.reducer;