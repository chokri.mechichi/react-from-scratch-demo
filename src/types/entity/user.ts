export type User = {
    id : number
    firstName : string
    lastName : string
    email : string
    phone : string
    gender : string
    image : string
}