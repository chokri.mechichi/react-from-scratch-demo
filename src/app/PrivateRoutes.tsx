import { useEffect, useState } from 'react'
import { Outlet, Navigate } from 'react-router-dom'
import { selectIsLoggedIn, loadLoggedUser } from '../slices/auth.slice'
import { useAppSelector, useAppDispatch } from './hooks'
const PrivateRoutes = () => {
  const dispatch = useAppDispatch()
  const isAllowed = useAppSelector(selectIsLoggedIn)
  const [checked, set_checked] = useState(false)

  useEffect(() => {
    dispatch(loadLoggedUser())
    set_checked(true)
  }, [])

  return checked ? isAllowed ? <Outlet /> : <Navigate to="/login" /> : <></>
}

export default PrivateRoutes
