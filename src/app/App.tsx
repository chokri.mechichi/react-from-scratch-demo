import React from 'react'
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom'
import PrivateRoutes from './PrivateRoutes'
import LoginContainer from '../features/login/Login.container'
import MainContainer from '../features/main/Main.container'
import '../ui/_setting/base.css'
const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        {/* ----------------Routes ---------------------*/}
        <Route path="/login" element={<LoginContainer />} />
        <Route path="*" element={<LoginContainer />} />
        <Route element={<PrivateRoutes />}>
          <Route path="app/*" element={<MainContainer />} />
        </Route>
        {/* ----------------Private Routes ---------------------*/}
      </Routes>
    </BrowserRouter>
  )
}

export default App
