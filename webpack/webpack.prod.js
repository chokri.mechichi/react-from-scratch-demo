const webpack = require('webpack')
const path = require('path')

module.exports = {
  mode: 'production',
  devtool: 'source-map',
  plugins: [
  ],
  output: {
    path: path.resolve(__dirname, '..', './build'),
    filename: 'bundle.js',
},
}